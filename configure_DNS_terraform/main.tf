# create DNS entry with Project Name
# Project name and IP will be sended from other stages using sed commande
resource "ovh_domain_zone_record" "test" {
    zone = "douda.ovh"
    subdomain = "##PROJECT##"
    fieldtype = "A"
    ttl = "3600"
    target = "##IP##"
}


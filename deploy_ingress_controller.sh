# google cloud authentification
gcloud auth activate-service-account --key-file=./configure_DNS_terraform/key.json
gcloud config set project amplified-wares-392313
gcloud config set container/cluster $CI_PROJECT_NAME 
gcloud config set compute/zone us-east1
# GKE authentification
gcloud container clusters get-credentials $CI_PROJECT_NAME --region us-east1-b --project amplified-wares-392313
# helm uninstall nginx-ingress
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx-ingress ingress-nginx/ingress-nginx
# this is default config validationc created when use helm chart to deploy ingress controller 
kubectl delete validatingwebhookconfigurations nginx-ingress-ingress-nginx-admission
# check services for ingress controller have been created ( one should contain EXTERNAL IP adress)
kubectl get services

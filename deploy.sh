#Google cloud authentification
gcloud auth activate-service-account --key-file=$KEY
gcloud config set project amplified-wares-392313
gcloud config set container/cluster $CI_PROJECT_NAME
gcloud config set compute/zone us-east1
# GKE authentification
gcloud container clusters get-credentials $CI_PROJECT_NAME --region us-east1-b --project amplified-wares-392313
# send Branche Name to ressources to be created
sed -i "s|##BRANCHE##|$(echo $CI_COMMIT_BRANCH | cut -c 9-)|g" deploy_app_k8s.yml
# Create or update ressources
kubectl apply -f deploy_app_k8s.yml
kubectl rollout restart deployment $(echo $CI_COMMIT_BRANCH | cut -c 9-)
# install ingress-rules plugin 
set -x; cd "$(mktemp -d)" &&
OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
KREW="krew-${OS}_${ARCH}" &&
curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
tar zxvf "${KREW}.tar.gz" && ./"${KREW}" install krew
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
kubectl krew install ingress-rule
kubectl ingress-rule
# apply ingress rule to expose the application
kubectl ingress-rule set $CI_PROJECT_NAME --service "$(echo $CI_COMMIT_BRANCH | cut -c 9-)" --port 80 --host $CI_PROJECT_NAME.douda.ovh --path /"$(echo $CI_COMMIT_BRANCH | cut -c 9-)(/|$)(.*)" --ingress-class nginx

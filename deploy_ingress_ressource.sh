# google cloud authentification
gcloud auth activate-service-account --key-file=./configure_DNS_terraform
gcloud config set project amplified-wares-392313
gcloud config set container/cluster $CI_PROJECT_NAME
gcloud config set compute/zone us-east1
# GKE authentification
gcloud container clusters get-credentials $CI_PROJECT_NAME --region us-west1-b --project amplified-wares-392313
# send PROJECT NAME to ingress ressources to be created
sed -i "s|PROJECT_NAME|$CI_PROJECT_NAME|g" ingress.yml
# create ingress ressources
kubectl apply -f ingress.yml
